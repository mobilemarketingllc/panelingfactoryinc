msgid ""
msgstr ""
"Project-Id-Version: BIALTY - Bulk Image Alt Text (Alt tag, Alt Attribute) "
"with Yoast SEO + WooCommerce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-17 12:46+0000\n"
"PO-Revision-Date: 2019-06-21 16:06+0200\n"
"Last-Translator: admin <key7i@yahoo.com>\n"
"Language-Team: German\n"
"Language: de_DE\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Loco-Version: 2.3.0; wp-5.1.1\n"

#: bulk-image-alt-text-with-yoast-admin.php:49
#, php-format
msgid "<a href=\"%s\">Get Pro version</a> to enable"
msgstr "<a href=\"%s\">Pro-Version holen</a> zum Aktivieren von"

#: bulk-image-alt-text-with-yoast-admin.php:100
msgid "Settings saved."
msgstr "Einstellungen gespeichert."

#: bulk-image-alt-text-with-yoast-admin.php:110
msgid "Settings"
msgstr "Einstellungen"

#: bulk-image-alt-text-with-yoast-admin.php:132
msgid ""
"Yoast SEO is either not installed or disabled. \"Focus Keyword\" won't work "
"(of course). \"Both...\" feature will add only Post titles to Alt tags"
msgstr ""
"Yoast SEO ist entweder nicht installiert oder deaktiviert. \"Focus Keyword\" "
"funktioniert nicht (natürlich). \"Beide...\" Funktion wird nur Post-Titel zu "
"den Alt-Tags hinzufügen."

#: bulk-image-alt-text-with-yoast-admin.php:139
msgid "BIALTY, how does it work?"
msgstr "BIALTY, wie funktioniert es?"

#: bulk-image-alt-text-with-yoast-admin.php:140
msgid "1. Select what to do with missing alt tags"
msgstr "1. Wählen Sie aus, was mit fehlenden Alt-Tags geschehen soll."

#: bulk-image-alt-text-with-yoast-admin.php:141
msgid "2. Select what to do with existing alt tags"
msgstr "2. Wählen Sie aus, was mit bestehenden Alt-Tags geschehen soll."

#: bulk-image-alt-text-with-yoast-admin.php:142
msgid ""
"3. Click on \"save changes\" and your settings will be applied everywhere."
msgstr ""
"3. Klicken Sie auf \"Änderungen speichern\" und Ihre Einstellungen werden "
"überall übernommen."

#: bulk-image-alt-text-with-yoast-admin.php:143
msgid ""
"4. Bialty will be active for all future publications which means that you "
"won't have to worry anymore about Alt texts."
msgstr ""
"4. Bialty wird für alle zukünftigen Veröffentlichungen aktiv sein, so dass "
"Sie sich keine Sorgen mehr um Alt-Texte machen müssen."

#: bulk-image-alt-text-with-yoast-admin.php:146
msgid "About Page and Post Alt texts"
msgstr "Über Page und Post Alt Texte"

#: bulk-image-alt-text-with-yoast-admin.php:151
#: bulk-image-alt-text-with-yoast-admin.php:205
#: bulk-image-alt-text-with-yoast-admin.php:257
msgid "Replace Missing Alt Tags with"
msgstr "Fehlende Alt-Tags ersetzern durch"

#: bulk-image-alt-text-with-yoast-admin.php:157
#: bulk-image-alt-text-with-yoast-admin.php:182
#: bulk-image-alt-text-with-yoast-admin.php:211
#: bulk-image-alt-text-with-yoast-admin.php:236
#: bulk-image-alt-text-with-yoast-admin.php:263
#: bulk-image-alt-text-with-yoast-admin.php:287
msgid "Yoast Focus Keyword"
msgstr "Yoast Focus Schlüsselwort"

#: bulk-image-alt-text-with-yoast-admin.php:160
#: bulk-image-alt-text-with-yoast-admin.php:185
#: bulk-image-alt-text-with-yoast-admin.php:214
#: bulk-image-alt-text-with-yoast-admin.php:239
#: bulk-image-alt-text-with-yoast-admin.php:266
#: bulk-image-alt-text-with-yoast-admin.php:290
msgid "Post Title"
msgstr "Beitragstitel"

#: bulk-image-alt-text-with-yoast-admin.php:163
#: bulk-image-alt-text-with-yoast-admin.php:188
#: bulk-image-alt-text-with-yoast-admin.php:217
#: bulk-image-alt-text-with-yoast-admin.php:242
#: bulk-image-alt-text-with-yoast-admin.php:269
#: bulk-image-alt-text-with-yoast-admin.php:293
msgid "Both Focus Keyword & Post Title"
msgstr "Sowohl Fokus Keyword als auch Post Title"

#: bulk-image-alt-text-with-yoast-admin.php:166
#: bulk-image-alt-text-with-yoast-admin.php:191
#: bulk-image-alt-text-with-yoast-admin.php:220
#: bulk-image-alt-text-with-yoast-admin.php:245
#: bulk-image-alt-text-with-yoast-admin.php:272
#: bulk-image-alt-text-with-yoast-admin.php:296
msgid "Disable"
msgstr "Deaktivieren"

#: bulk-image-alt-text-with-yoast-admin.php:175
#: bulk-image-alt-text-with-yoast-admin.php:229
#: bulk-image-alt-text-with-yoast-admin.php:281
msgid "Replace Defined Alt Tags with"
msgstr "Definierte Alt-Tags ersetzen durch"

#: bulk-image-alt-text-with-yoast-admin.php:198
msgid "About Product Alt texts (for Woocommerce)"
msgstr "Über Produkt Alt Texte (für Woocommerce)"

#: bulk-image-alt-text-with-yoast-admin.php:303
msgid "Bulk Image Alt Text for Woocommerce Products"
msgstr "Großbild Alt Text für Woocommerce Produkte"

#: bulk-image-alt-text-with-yoast-admin.php:316
msgid "Add Site Title"
msgstr "Site-Titel hinzufügen"

#: bulk-image-alt-text-with-yoast-admin.php:330
msgid ""
"Add website title defined in Settings &raquo; General to alt text as well"
msgstr ""
"Hinzufügen eines Website-Titels, der in Einstellungen &raquo; Allgemein "
"definiert ist, um auch den Text zu ändern."

#: bulk-image-alt-text-with-yoast-admin.php:340
msgid "Delete Settings"
msgstr "Einstellungen löschen"

#: bulk-image-alt-text-with-yoast-admin.php:354
msgid "Checking this box will remove all settings when you deactivate plugin."
msgstr ""
"Wenn Sie dieses Kontrollkästchen aktivieren, werden alle Einstellungen "
"entfernt, wenn Sie das Plugin deaktivieren."

#: bulk-image-alt-text-with-yoast-admin.php:364
msgid "Boost your ranking on Search engines"
msgstr "Steigern Sie Ihr Ranking in Suchmaschinen"

#: bulk-image-alt-text-with-yoast-admin.php:376
msgid "Optimize site's crawlability with an optimized robots.txt"
msgstr ""
"Optimieren Sie die Crawlability der Website mit einer optimierten robots.txt."

#: bulk-image-alt-text-with-yoast-admin.php:381
#, php-format
msgid ""
"Click <a href=\"%s\" target=\"_blank\">HERE</a> to Install <a href=\"%2s\" "
"target=\"_blank\">Better Robots.txt plugin</a> to boost your robots.txt"
msgstr ""
"Klicken Sie auf <a href=\"%s\" target=\"_blank\">HIER</a>, um <a href=\"%2s"
"\" target=\"_blank\">Better Robots.txt plugin</a> zu installieren, um Ihre "
"robots.txt zu erhöhen."

#: bulk-image-alt-text-with-yoast-admin.php:401
msgid "Mobile-Friendly & responsive design"
msgstr "Mobilfreundliches und reaktionsschnelles Design"

#: bulk-image-alt-text-with-yoast-admin.php:413
msgid ""
"Get dynamic mobile previews of your pages/posts/products + Facebook debugger"
msgstr ""
"Erhalten Sie dynamische mobile Vorschauen Ihrer Seiten/Posts/Produkte + "
"Facebook Debugger"

#: bulk-image-alt-text-with-yoast-admin.php:418
#, php-format
msgid ""
"Click <a href=\"%s\" target=\"_blank\">HERE</a> to Install <a href=\"%2s\" "
"target=\"_blank\">Mobilook</a> and test your website on Dualscreen format "
"(Galaxy fold)"
msgstr ""
"Klicken Sie auf <a href=\"%s\" target=\"_blank\">HIER</a>, um <a href=\"%2s"
"\" target=\"_blank\">Mobilook</a> zu installieren und Ihre Website auf "
"Dualscreen-Format (Galaxy-Falz) zu testen."

#: bulk-image-alt-text-with-yoast-admin.php:434
msgid "Save Changes"
msgstr "Änderungen speichern"

#: bulk-image-alt-text-with-yoast-admin.php:437
msgid "How to check your Alt texts now?"
msgstr "Wie kann man jetzt seine Alt-Texte überprüfen?"

#: bulk-image-alt-text-with-yoast-admin.php:438
msgid ""
"Go to your website, click right on a webpage and select \"Show Page Source."
"\" (Firefox, Safari, Chrome, Internet Explorer,...). Scroll down to the "
"appropriate section (displaying your content), after header area and before "
"footer area. You will be able to identify your modified Alt Texts with your "
"post title (if selected), your Yoast's Focus Keyword (if used) and your site "
"name (if selected), separated with a comma. Please note that BIALTY modifies "
"image Alt texts on Frontend (in your HTML code), not on backend (Media "
"LIbrary, etc.), which would be useless for search engines. Want more details "
"about this? Check our video :"
msgstr ""
"Gehen Sie zu Ihrer Website, klicken Sie mit der rechten Maustaste auf eine "
"Webseite und wählen Sie \"Seitenquelle anzeigen\". (Firefox, Safari, Chrome, "
"Internet Explorer,....). Blättern Sie nach unten zum entsprechenden "
"Abschnitt (der Ihren Inhalt anzeigt), nach dem Kopfbereich und vor dem "
"Fußbereich. Sie können Ihre geänderten Alt-Texte anhand Ihres Post-Titels "
"(falls ausgewählt), Ihres Yoast's Focus Keywords (falls verwendet) und Ihres "
"Site-Namens (falls ausgewählt) identifizieren, getrennt durch ein Komma. "
"Bitte beachten Sie, dass BIALTY Bild-Alt-Texte am Frontend (in Ihrem HTML-"
"Code) und nicht am Backend (Medienbibliothek, etc.) ändert, was für "
"Suchmaschinen nutzlos wäre. Möchten Sie mehr darüber erfahren? Schauen Sie "
"sich unser Video an:"

#: bulk-image-alt-text-with-yoast-admin.php:442
msgid ""
"IMPORTANT: BIALTY plugin modifies image alt texts on front-end. Any empty or "
"existing alt text will be replaced according to settings above. About Yoast "
"SEO, please note that it \"checks\" content in real time inside text editor "
"in Wordpress back-end, so even if Yoast does not display a green bullet for "
"the \"image alt attributes\" line, BIALTY is still doing the job. For your "
"information, Google Bot and other search engine bots see only image alt "
"attributes on Front-end (not as Yoast reading content inside text editor)"
msgstr ""
"WICHTIG: BIALTY Plugin modifiziert Image-Alt-Texte am Frontend. Jeder leere "
"oder vorhandene Alt-Text wird entsprechend den obigen Einstellungen ersetzt. "
"Über Yoast SEO, beachten Sie bitte, dass es Inhalte in Echtzeit im "
"Texteditor im Wordpress Backend \"überprüft\", so dass selbst wenn Yoast "
"keinen grünen Punkt für die Zeile \"image alt attributes\" anzeigt, BIALTY "
"immer noch die Arbeit macht. Zu Ihrer Information, Google Bot und andere "
"Suchmaschinen-Bots sehen nur Bild-Alt-Attribute auf dem Frontend (nicht als "
"Yoast reading content im Texteditor)."

#: bulk-image-alt-text-with-yoast-admin.php:446
msgid ""
"Note 1: BIALTY is fully compatible with most popular page builders (TinyMCE, "
"SiteOrigin, Elementor, Gutenberg)"
msgstr ""
"Hinweis 1: BIALTY ist voll kompatibel mit den meisten gängigen "
"Seitenerstellern (TinyMCE, SiteOrigin, Elementor, Gutenberg)."

#: bulk-image-alt-text-with-yoast-admin.php:447
msgid ""
"Note 2: If you've installed YOAST SEO but did not optimize yet, select "
"\"Both Focus Keyword & Post title\""
msgstr ""
"Hinweis 2: Wenn Sie YOAST SEO installiert haben, aber noch nicht optimiert "
"haben, wählen Sie \"Sowohl Fokus Keyword als auch Post-Titel\"."

#: bulk-image-alt-text-with-yoast-admin.php:448
msgid ""
"Note 3: If you did not install YOAST SEO plugin, please keep default "
"settings. BIALTY will add your post titles to Alt tags."
msgstr ""
"Hinweis 3: Wenn Sie das YOAST SEO Plugin nicht installiert haben, behalten "
"Sie bitte die Standardeinstellungen bei. BIALTY fügt Ihre Post-Titel zu den "
"Alt-Tags hinzu."

#: bulk-image-alt-text-with-yoast-admin.php:525
msgid "Use Custom Alt Text for all images?*"
msgstr "Benutzerdefinierten Alt-Text für alle Bilder verwenden?*"

#: bulk-image-alt-text-with-yoast-admin.php:530
#: bulk-image-alt-text-with-yoast-admin.php:552
msgid "Yes"
msgstr "Ja"

#: bulk-image-alt-text-with-yoast-admin.php:533
#: bulk-image-alt-text-with-yoast-admin.php:555
msgid "No"
msgstr "Nein"

#: bulk-image-alt-text-with-yoast-admin.php:542
msgid ""
"Insert your custom Alt text (other than Yoast Focus Keyword & page title)"
msgstr ""
"Fügen Sie Ihren eigenen Alt-Text ein (außer Yoast Focus Keyword & "
"Seitentitel)."

#: bulk-image-alt-text-with-yoast-admin.php:547
msgid "Disable Bialty?"
msgstr "Bialty deaktivieren?"

#: bulk-image-alt-text-with-yoast-admin.php:559
msgid "*If NO, default Bialty settings will be applied"
msgstr "*Wenn NEIN, werden die Standardeinstellungen von Bialty übernommen."

#: bulk-image-alt-text-with-yoast.php:79
#, php-format
msgid ""
"Hey %1$s, %2$s Click on Allow & Continue to start optimizing your images "
"with ALT tags :)!  Don't spend hours at adding manually alt tags to your "
"images. BIALTY will use your YOAST settings automatically to get better "
"results on search engines and improve your SEO. %2$s Never miss an important "
"update -- opt-in to our security and feature updates notifications. %2$s See "
"you on the other side."
msgstr ""
"Hey %1$s, %2$s Klicken Sie auf Erlauben & Fortfahren, um mit der Optimierung "
"Ihrer Bilder mit ALT-Tags zu beginnen :)!  Verbringen Sie keine Stunden "
"damit, Ihren Bildern manuell geänderte Tags hinzuzufügen. BIALTY verwendet "
"Ihre YOAST-Einstellungen automatisch, um bessere Ergebnisse in Suchmaschinen "
"zu erzielen und Ihre SEO zu verbessern. %2$s Verpassen Sie nie ein wichtiges "
"Update - melden Sie sich bei unseren Benachrichtigungen über Sicherheits- "
"und Feature-Updates an. %2$s Wir sehen uns auf der anderen Seite."

#: inc/faq.php:5
msgid "1.What is alt text?"
msgstr "1. was ist Alt-Text?"

#: inc/faq.php:9
msgid ""
"Alt text (alternative text), also known as \"alt attributes\", “alt "
"descriptions,” and technically incorrectly as \"alt tags,” are used within "
"an HTML code to describe the appearance and function of an image on a page."
msgstr ""
"Alt-Text (Alternativtext), auch bekannt als \"alt-Attribute\", \"alt-"
"Beschreibungen\" und technisch falsch als \"alt-Tags\", werden innerhalb "
"eines HTML-Codes verwendet, um das Aussehen und die Funktion eines Bildes "
"auf einer Seite zu beschreiben."

#: inc/faq.php:12
msgid "How to use Alt text:"
msgstr "Wie man Alt-Text verwendet:"

#: inc/faq.php:15
msgid ""
"Adding alternative text to photos is first and foremost a principle of web "
"accessibility. Visually impaired users using screen readers will be read an "
"alt attribute to better understand an on-page image."
msgstr ""
"Das Hinzufügen von Alternativtext zu Fotos ist in erster Linie ein Prinzip "
"der Barrierefreiheit im Internet. Sehbehinderte Benutzer, die Screenreader "
"verwenden, werden ein alt-Attribut lesen, um ein Bild auf der Seite besser "
"zu verstehen."

#: inc/faq.php:16
msgid ""
"Alt tags will be displayed in place of an image if an image file cannot be "
"loaded."
msgstr ""
"Alt-Tags werden anstelle eines Bildes angezeigt, wenn eine Bilddatei nicht "
"geladen werden kann."

#: inc/faq.php:17
msgid ""
"Alt tags provide better image context/descriptions to search engine "
"crawlers, helping them to index an image properly."
msgstr ""
"Alt-Tags bieten Suchmaschinen-Crawlern einen besseren Bildkontext/"
"Beschreibungen und helfen ihnen, ein Bild richtig zu indizieren."

#: inc/faq.php:26
msgid "Appropriate length?"
msgstr "Angemessene Länge?"

#: inc/faq.php:30
msgid ""
"Google seemed to count the first 16 words in the ALT tag and interestingly "
"in the snippet Google uses, it does seem to completely cut off the rest of "
"the ALT and from the 17th word. Having 16 words to work with might prove "
"very useful if you are using ALT tags to describe more complex images. There "
"is potentially plenty of available space to describe images properly for "
"accessibility purposes AND SEO impact."
msgstr ""
"Google schien die ersten 16 Wörter im ALT-Tag zu zählen und "
"interessanterweise im Ausschnitt, den Google verwendet, scheint es den Rest "
"des ALT und das 17. Wort vollständig abzuschneiden. 16 Wörter zum Arbeiten "
"zu haben, könnte sich als sehr nützlich erweisen, wenn Sie ALT-Tags "
"verwenden, um komplexere Bilder zu beschreiben. Es steht möglicherweise "
"genügend Platz zur Verfügung, um Bilder für Barrierefreiheit und SEO-"
"Auswirkungen richtig zu beschreiben."

#: inc/faq.php:39
msgid "How Image Alt Tags and Meta Data Help SEO"
msgstr "Wie Image Alt Tags und Metadaten SEO unterstützen"

#: inc/faq.php:43
msgid ""
"Optimizing your images for SEO helps crawlers better index your web pages, "
"which in turn can give you a rankings boost because it can make the page "
"more relevant to users."
msgstr ""
"Die Optimierung Ihrer Bilder für SEO hilft Crawlern, Ihre Webseiten besser "
"zu indizieren, was wiederum zu einem Anstieg der Rankings führen kann, da es "
"die Seite für die Benutzer relevanter machen kann."

#: inc/faq.php:46
msgid ""
"Let’s say a searcher needs plumber repairs for a clogged bathroom drain. "
"Google has to choose between two web pages from different companies, both of "
"which have equal ranking factors."
msgstr ""
"Nehmen wir an, ein Sucher braucht eine Klempnerreparatur für einen "
"verstopften Badezimmerablauf. Google muss sich zwischen zwei Webseiten "
"verschiedener Unternehmen entscheiden, die beide gleichrangige Faktoren "
"haben."

#: inc/faq.php:49
msgid ""
"As the crawler reads through the first page, it doesn’t identify any image "
"alt-tags, therefore, it assumes the images (if there are any) do not add "
"page-specific value. On the second page, however, the crawler locates five "
"images, each one with a full description of what the image is showing. All "
"five images’ alt tags supplement the rest of the text on the page."
msgstr ""
"Wenn der Crawler die erste Seite durchliest, identifiziert er keine Bild-Alt-"
"Tags, daher geht er davon aus, dass die Bilder (falls vorhanden) keinen "
"seitenspezifischen Wert hinzufügen. Auf der zweiten Seite findet der Crawler "
"jedoch fünf Bilder, die jeweils eine vollständige Beschreibung dessen "
"enthalten, was das Bild zeigt. Die Alt-Tags aller fünf Bilder ergänzen den "
"Rest des Textes auf der Seite."

#: inc/faq.php:52
msgid ""
"Since Google is all about spitting out the results you’ll most likely jive "
"with, it’s going to go with the article that it thinks is more relevant."
msgstr ""
"Da es bei Google nur darum geht, die Ergebnisse auszuspucken, mit denen Sie "
"höchstwahrscheinlich leben werden, wird es zu dem Artikel passen, von dem es "
"denkt, dass er relevanter ist."

#: inc/notices.php:35
#, php-format
msgid ""
"Show support for BIALTY - Bulk Image Alt Text (Alt tag, Alt Attribute) with "
"Yoast SEO + WooCommerce with a 5-star rating » <a href=\"%s\" target=\"_blank"
"\">Click here</a>"
msgstr ""
"Unterstützung für BIALTY anzeigen - Bulk Image Alt Text (Alt Tag, Alt "
"Attribute) mit Yoast SEO + WooCommerce mit 5-Sterne-Bewertung \" <a href=\"%s"
"\" target=\"_blank\">Klicken Sie hier</a>."

#: inc/recommendations.php:4
msgid "Schema App Structured Data by Hunch Manifest"
msgstr "Schema App Structured Data von Hunch Manifest"

#: inc/recommendations.php:5
msgid ""
"Get Schema.org structured data for all pages, posts, categories and profile "
"pages on activation."
msgstr ""
"Erhalten Sie strukturierte Daten für alle Seiten, Beiträge, Kategorien und "
"Profilseiten von Schema.org bei der Aktivierung."

#: inc/recommendations.php:10
msgid "Yasr – Yet Another Stars Rating by Dario Curvino"
msgstr "Yasr – Yet Another Stars Rating von Dario Curvino"

#: inc/recommendations.php:11
msgid ""
"Boost the way people interact with your website, e-commerce or blog with an "
"easy and intuitive WordPress rating system!"
msgstr ""
"Steigern Sie die Art und Weise, wie Menschen mit Ihrer Website, Ihrem E-"
"Commerce oder Blog interagieren, mit einem einfachen und intuitiven "
"WordPress Bewertungssystem!"

#: inc/recommendations.php:16
msgid ""
"Better Robots.txt optimization – Website indexing, traffic, ranking & SEO "
"Booster + Woocommerce"
msgstr ""
"Bessere Robots.txt Optimierung - Website Indexierung, Traffic, Ranking & SEO "
"Booster + Woocommerce"

#: inc/recommendations.php:17
msgid ""
"Better Robots.txt is an all in one SEO robots.txt plugin, it creates a "
"virtual robots.txt including your XML sitemaps (Yoast or else) to boost your "
"website ranking on search engines."
msgstr ""
"Better Robots.txt ist ein all in one SEO robots.txt Plugin, es erstellt eine "
"virtuelle robots.txt mit Ihren XML-Sitemaps (Yoast oder sonst), um Ihr "
"Website-Ranking in Suchmaschinen zu verbessern."

#: inc/recommendations.php:22
msgid "Smush Image Compression and Optimization By WPMU DEV"
msgstr "Smush Image Compression and Optimization von WPMU DEV"

#: inc/recommendations.php:23
msgid ""
"Compress and optimize (or optimise) image files, improve performance and "
"boost your SEO rank using Smush WordPress image compression and optimization."
msgstr ""
"Komprimieren und optimieren (oder optimieren) Sie Bilddateien, verbessern "
"Sie die Leistung und steigern Sie Ihren SEO-Rang mit Smush WordPress "
"Bildkompression und -optimierung."

#: inc/recommendations.php:28
msgid "404 to 301 By Joel James"
msgstr "404 bis 301 von Joel James"

#: inc/recommendations.php:29
msgid ""
"Automatically redirect, log and notify all 404 page errors to any page using "
"301 redirection..."
msgstr ""
"Automatische Umleitung, Protokollierung und Benachrichtigung aller 404 "
"Seitenfehler auf jede Seite mit 301 Umleitung....."

#: inc/recommendations.php:34
msgid "Yoast SEO By Team Yoast"
msgstr "Yoast SEO von Team Yoast"

#: inc/recommendations.php:35
msgid ""
"Improve your WordPress SEO: Write better content and have a fully optimized "
"WordPress site using the Yoast SEO plugin."
msgstr ""
"Verbessern Sie Ihre WordPress SEO: Schreiben Sie bessere Inhalte und haben "
"Sie eine vollständig optimierte WordPress-Seite mit dem Yoast SEO Plugin."

#: inc/recommendations.php:43
msgid "Top plugins for SEO performance:"
msgstr "Top-Plugins für SEO-Performance:"

#: inc/recommendations.php:49
msgid ""
"BIALTY - Bulk Image Alt Text by Pagup provides a selection of plugins "
"allowing to keep your website healthy, get better results on Search engines "
"and increase your sales for ecommerce solutions."
msgstr ""
"BIALTY - Bulk Image Alt Text von Pagup bietet eine Auswahl an Plugins, die "
"es ermöglichen, Ihre Website gesund zu halten, bessere Ergebnisse in "
"Suchmaschinen zu erzielen und Ihren Umsatz für E-Commerce-Lösungen zu "
"steigern."

#: inc/recommendations.php:69 inc/recommendations.php:175
msgid "Download"
msgstr "Herunterladen"

#: inc/recommendations.php:81
msgid "WP-Optimize by David Anderson, Ruhani Rabin, Team Updraft"
msgstr "WP-Optimize von David Anderson, Ruhani Rabin, Team Updraft"

#: inc/recommendations.php:82
msgid ""
"WP-Optimize is WordPress's most-installed optimization plugin. With it, you "
"can clean up your database easily and safely, without manual queries."
msgstr ""
"WP-Optimize ist WordPress's am häufigsten installiertes Optimierungs-Plugin. "
"Mit ihm können Sie Ihre Datenbank einfach und sicher bereinigen, ohne "
"manuelle Abfragen."

#: inc/recommendations.php:87
msgid "WordPress Share Buttons Plugin – AddThis By The AddThis Team"
msgstr "WordPress Share Buttons Plugin - AddThis vom AddThis Team"

#: inc/recommendations.php:88
msgid ""
"Share buttons from AddThis help you get more traffic from sharing through "
"social networks."
msgstr ""
"Share Buttons von AddThis helfen Ihnen, mehr Traffic durch die Freigabe in "
"sozialen Netzwerken zu erhalten."

#: inc/recommendations.php:93
msgid "WP-SpamShield WordPress Anti-Spam Plugin by Red Sand Media Group"
msgstr "WP-SpamShield WordPress Anti-Spam Plugin von Red Sand Media Group"

#: inc/recommendations.php:94
msgid ""
"WP-SpamShield is a leading WordPress anti-spam plugin that stops spam "
"instantly and improves your site's security. "
msgstr ""
"WP-SpamShield ist ein führendes WordPress Anti-Spam-Plugin, das Spam sofort "
"stoppt und die Sicherheit Ihrer Website verbessert. "

#: inc/recommendations.php:99
msgid "OneSignal – Free Web Push Notifications By OneSignal"
msgstr "OneSignal – Free Web Push Notifications von OneSignal"

#: inc/recommendations.php:100
msgid ""
"Increase engagement and drive more repeat traffic to your WordPress site "
"with desktop push notifications. Now supporting Chrome, Firefox, and Safari."
msgstr ""
"Erhöhen Sie das Engagement und steigern Sie den Traffic auf Ihrer WordPress-"
"Seite mit Desktop-Push-Benachrichtigungen. Unterstützt jetzt Chrome, Firefox "
"und Safari."

#: inc/recommendations.php:105
msgid "WPfomify - Social Proof & FOMO Marketing Plugin"
msgstr "WPfomify - Social Proof & FOMO Marketing Plugin"

#: inc/recommendations.php:106
msgid ""
"WPfomify increases conversion rates on your websites by displaying recent "
"interaction, sales and sign-ups"
msgstr ""
"WPfomify erhöht die Konversionsraten auf Ihren Websites, indem es die "
"letzten Interaktionen, Verkäufe und Anmeldungen anzeigt."

#: inc/recommendations.php:111
msgid "Recart - Abandoned Cart Toolbox"
msgstr "Recart - Abandoned Cart Toolbox"

#: inc/recommendations.php:112
msgid ""
"Recart Messenger Marketing and Abandoned Cart Toolbox is a All in one cart "
"recovery marketing tools for ecommerce solution"
msgstr ""
"Recart Messenger Marketing and Abandoned Cart Toolbox ist ein All-in-One-"
"Wagen Recovery Marketing-Tools für E-Commerce-Lösung"

#: inc/recommendations.php:117
msgid "Wp-Roket - Probably the best caching Plugin for WordPress"
msgstr "Wp-Roket - Wahrscheinlich das beste Caching-Plugin für WordPress."

#: inc/recommendations.php:118 inc/recommendations.php:136
msgid ""
"Speed up your WordPress website, more traffic, conversions and money with WP "
"Rocket caching plugin."
msgstr ""
"Beschleunigen Sie Ihre WordPress Website, mehr Traffic, Konvertierungen und "
"Geld mit dem WP Rocket Caching Plugin."

#: inc/recommendations.php:123
msgid "Nofollow for external link By CyberNetikz"
msgstr "Nofollow for external link von CyberNetikz"

#: inc/recommendations.php:124
msgid ""
"Automatically insert rel=nofollow and target=_blank to all the external "
"links into your website posts, pages or menus. Support exclude domain."
msgstr ""
"Fügen Sie rel=nofollow und target=_blank automatisch zu allen externen Links "
"in Ihre Webseitenbeiträge, Seiten oder Menüs ein. Unterstützt exclude domain."

#: inc/recommendations.php:129
msgid "Google Reviews Widget By RichPlugins"
msgstr "Google Reviews Widget von RichPlugins"

#: inc/recommendations.php:130
msgid ""
"Google Reviews Widget show Google Places Reviews on your WordPress website "
"to increase user confidence and SEO."
msgstr ""
"Google Reviews Widget zeigt Google Places Reviews auf Ihrer WordPress "
"Website an, um das Vertrauen der Benutzer und die Suchmaschinenoptimierung "
"zu erhöhen."

#: inc/recommendations.php:135
msgid "WP Chatbot for facebook Messenger customer chat By HoliThemes"
msgstr "WP Chatbot for facebook Messenger customer chat von HoliThemes"

#: inc/recommendations.php:141
msgid "WP Google My Business Auto Publish By Martin Gibson"
msgstr "WP Google My Business Auto Publish von Martin Gibson"

#: inc/recommendations.php:142
msgid ""
"WP Google My Business Auto Publish lets you publish posts, custom posts and "
"pages automatically from WordPress to your Google My Business page."
msgstr ""
"Mit WP Google My Business Auto Publish können Sie Beiträge, "
"benutzerdefinierte Beiträge und Seiten automatisch aus WordPress auf Ihrer "
"Google My Business-Seite veröffentlichen."

#: inc/recommendations.php:147
msgid "WP Search Console"
msgstr "WP Search Console"

#: inc/recommendations.php:148
msgid ""
"A new way to boost your marketing content. Writing Web Content that is "
"ranking like those of SEO Experts."
msgstr ""
"Eine neue Art, Ihre Marketinginhalte zu verbessern. Schreiben von "
"Webinhalten, die ähnlich wie die von SEO-Experten rangieren."

#: inc/recommendations.php:156
msgid "Awesome plugins for SEO & Conversion performance:"
msgstr "Fantastische Plugins für SEO & Conversion:"

#: inc/recommendations.php:186
msgid ""
"Upgrade to PRO version to UNLOCK 12 additional awesome plugins "
"recommendations for SEO & Conversion performance"
msgstr ""
"Upgrade auf die PRO-Version auf UNLOCK 12 zusätzliche fantastische Plugin-"
"Empfehlungen für SEO & Conversion"

#: inc/recommendations.php:192
msgid ""
"Want to suggest another plugin ? ... Send us a message at support@better-"
"robots.com"
msgstr ""
"Möchte ein anderes Plugin vorschlagen ? ..... Senden Sie uns eine Nachricht "
"an support@better-robots.com"

#: inc/seo-recommendations.php:1
msgid "SEO tools recommended by BIALTY !:"
msgstr "SEO tools recommended von BIALTY !:"

#: inc/seo-recommendations.php:5
msgid "Premium Feature"
msgstr "Premium-Feature"

#: inc/seo-recommendations.php:12
msgid "Loading performance"
msgstr "Leistung laden"

#: inc/seo-recommendations.php:13
msgid ""
"Get insight on how well your site loads with actionable recommendations on "
"how to optimize it."
msgstr ""
"Verschaffen Sie sich einen Überblick darüber, wie gut Ihre Website belastet "
"wird, und geben Sie konkrete Empfehlungen zur Optimierung."

#: inc/seo-recommendations.php:16 inc/seo-recommendations.php:18
msgid "Analyse my site"
msgstr "Meine Website analysieren"

#: inc/seo-recommendations.php:26
msgid "Google Ranking"
msgstr "Google Ranking"

#: inc/seo-recommendations.php:27
msgid ""
"Analyse your Google ranking with keywords, traffic and more (REGISTER TO GET "
"10 FREE REQUESTS)"
msgstr ""
"Analysieren Sie Ihr Google-Ranking mit Keywords, Traffic und mehr "
"(REGISTRIEREN SIE sich, um 10 kostenlose Anfragen zu erhalten)."

#: inc/seo-recommendations.php:30 inc/seo-recommendations.php:32
msgid "My Google Ranking"
msgstr "My Google Ranking"

#: inc/seo-recommendations.php:44
msgid "Dead links"
msgstr "Tote Links"

#: inc/seo-recommendations.php:45
msgid ""
"Dead links, 404 errors, 503 errors, ... all bad links that could hurt your "
"website ranking."
msgstr ""
"Tote Links, 404 Fehler, 503 Fehler, .... alle schlechten Links, die Ihr "
"Website-Ranking beeinträchtigen könnten."

#: inc/seo-recommendations.php:48 inc/seo-recommendations.php:50
msgid "Check my links"
msgstr "Überprüfen Sie meine Links"

#: inc/seo-recommendations.php:58
msgid "SEO Score"
msgstr "SEO Score"

#: inc/seo-recommendations.php:59
msgid ""
"Get a simple and professional-quality SEO audit of your website for FREE"
msgstr ""
"Erhalten Sie ein einfaches und professionelles SEO-Audit Ihrer Website "
"kostenlos."

#: inc/seo-recommendations.php:62 inc/seo-recommendations.php:64
msgid "My SEO profile"
msgstr "Mein SEO-Profil"

#: inc/seo-recommendations.php:80
msgid "Index Your Website (Google)"
msgstr "Indizieren Sie Ihre Website (Google)"

#: inc/seo-recommendations.php:81
msgid ""
"Submit your website to Google Search Console and enjoy the real benefit of "
"Better Robots.txt"
msgstr ""
"Senden Sie Ihre Website an die Google Search Console und genießen Sie den "
"echten Nutzen von Better Robots.txt."

#: inc/seo-recommendations.php:82
msgid "Google Search Console"
msgstr "Google Search Console"

#: inc/seo-recommendations.php:88
msgid "Index your Website (Bing)"
msgstr "Indizieren Sie Ihre Website (Bing)"

#: inc/seo-recommendations.php:89
msgid ""
"Submit your website to Bing Webmaster tool and enjoy the real benefit of "
"Better Robots.txt"
msgstr ""
"Senden Sie Ihre Website an das Bing Webmaster-Tool und genießen Sie den "
"echten Nutzen von Better Robots.txt."

#: inc/seo-recommendations.php:90
msgid "Bing Webmaster tool"
msgstr "Bing Webmaster-Tool"

#: inc/sidebar.php:7
msgid "Subscribe & get a 10% OFF"
msgstr "Abonnieren & 10% Rabatt erhalten"

#: inc/sidebar.php:8
#, php-format
msgid ""
"Enter your email to get our SEO best practice guide AND a 10% coupon code on "
"BIALTY for Woocommerce plugin."
msgstr ""
"Geben Sie Ihre E-Mail-Adresse ein, um unseren SEO Best Practice Guide UND "
"einen 10%igen Gutscheincode auf BIALTY for Woocommerce Plugin zu erhalten."

#: inc/sidebar.php:13
msgid "Email address"
msgstr "E-Mail-Adresse"

#: inc/sidebar.php:16
msgid "Subscribe"
msgstr "Abonnieren"

#: inc/sidebar.php:24
msgid "BOOST YOUR RANKING"
msgstr "STEIGERN SIE IHR RANKING"

#: inc/sidebar.php:25
msgid ""
"Optimize your Robots.txt with Better Robots.txt (made by PAGUP) & Boost your "
"ranking on search engines."
msgstr ""
"Optimieren Sie Ihre Robots.txt mit Better Robots.txt (made by PAGUP) und "
"steigern Sie Ihr Ranking in Suchmaschinen."

#: inc/sidebar.php:26
msgid "Try Better Robots.txt &raquo;"
msgstr "Probieren Sie Better Robots.txt &raquo;"

#. Name of the plugin
msgid ""
"BIALTY - Bulk Image Alt Text (Alt tag, Alt Attribute) with Yoast SEO + "
"WooCommerce"
msgstr ""
"BIALTY - Bulk Image Alt Text (Alt Tag, Alt Attribut) mit Yoast SEO + "
"WooCommerce"

#. Description of the plugin
msgid ""
"Auto-add Alt texts, also called Alt Tags or Alt Attributes, from YOAST SEO "
"Focus Keyword field (or page/post/product title) with your page/post/product "
"title, to all images contained on your pages, posts, products, portfolios "
"for better Google Ranking on search engines – Fully compatible with "
"Woocommerce"
msgstr ""
"Automatische Hinzufügung von Alt-Texten, auch Alt-Tags oder Alt-Attribute "
"genannt, vom Feld YOAST SEO Focus Keyword (oder Page/Post/Produkttitel) mit "
"Ihrer Seite/Post/Produkttitel, bis hin zu allen Bildern, die auf Ihren "
"Seiten, Beiträgen, Produkten, Portfolios für ein besseres Google-Ranking in "
"Suchmaschinen enthalten sind - Voll kompatibel mit Woocommerce"

#. Author of the plugin
msgid "Pagup"
msgstr "Pagup"

#. Author URI of the plugin
msgid "https://pagup.com/"
msgstr "https://pagup.com/"
